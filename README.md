# sphereFACE #
*sphereFACE is a retro-3D vector shooter inspired by Asteroids, wrapped around the inside of a sphere.*

An unashamedly abstract shooter that ranges from slow, strategic sniping to frantic fast-paced rock-dodging action, with roguelike elements of exploration, progression, discovery and sudden death!

## Info ##
* http://sphereface.com

## Store pages ##
* http://voxelstorm.itch.io/sphereface
* http://store.steampowered.com/app/485680/

## Bug reporting ##
* http://code.voxelstorm.com/sphereface/issues